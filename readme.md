# HCNXBase

[![CI Status](http://img.shields.io/travis/Guillaume MARTINEZ/HCNXBase.svg?style=flat)](https://travis-ci.org/Guillaume MARTINEZ/HCNXBase)
[![Version](https://img.shields.io/cocoapods/v/HCNXBase.svg?style=flat)](http://cocoapods.org/pods/HCNXBase)
[![License](https://img.shields.io/cocoapods/l/HCNXBase.svg?style=flat)](http://cocoapods.org/pods/HCNXBase)
[![Platform](https://img.shields.io/cocoapods/p/HCNXBase.svg?style=flat)](http://cocoapods.org/pods/HCNXBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HCNXBase is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "HCNXBase"
```

## Author

Guillaume MARTINEZ, g.martinez@highconnexion.com

## License

HCNXBase is available under the MIT license. See the LICENSE file for more info.
